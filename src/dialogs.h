/*
 * By Tony Houghton, <h@realh.co.uk>.
 */

#ifndef DIALOGS_H
#define DIALOGS_H

/* Dialogs for XDG Autostart capplet */

#include <gtk/gtk.h>

void message_dialog(GtkWindow *parent, GtkMessageType,
		const char *message, ...);

#endif /* DIALOGS_H */
