/*
 * By Tony Houghton, <h@realh.co.uk>.
 */

#include "config.h"

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "dialogs.h"
#include "xxdgautostart.h"

DesktopEntry *xdesktop_entry_new(const char *pathname, const char *basename,
		gboolean savable)
{
	DesktopEntry *xde = g_new0(DesktopEntry, 1);

	if (!desktop_entry_load(xde, pathname, basename, TRUE, savable))
	{
		desktop_entry_delete(xde);
		xde = NULL;
	}
	return xde;
}

gboolean xdesktop_entry_get_only_in_rox(DesktopEntry *xde)
{
	return (xde->only_show_in && xde->only_show_in[0] &&
			!strcmp(xde->only_show_in[0], "ROX") &&
			(!xde->only_show_in[1] || !xde->only_show_in[1][0]));
}

gboolean xdesktop_entry_can_set_only_in_rox(DesktopEntry *xde)
{
	return xdesktop_entry_get_only_in_rox(xde) ||
		(desktop_entry_get_show_in_rox(xde) && 
		str_v_is_empty(xde->only_show_in));
}

static int str_v_len(char const **str_v)
{
	int n;

	for (n = 0; str_v && str_v[n]; ++n);
	return n;
}

static void update_kf_from_show_ins(DesktopEntry *xde)
{
	if (!xde->only_show_in)
	{
		g_key_file_remove_key(xde->kf, _DE, "OnlyShowIn", NULL);
	}
	else
	{
		g_key_file_set_string_list(xde->kf, _DE, "OnlyShowIn",
				(char const **) xde->only_show_in,
				str_v_len((char const **) xde->only_show_in));
	}
	if (!xde->not_show_in)
	{
		g_key_file_remove_key(xde->kf, _DE, "NotShowIn", NULL);
	}
	else
	{
		g_key_file_set_string_list(xde->kf, _DE, "NotShowIn",
				(char const **) xde->not_show_in,
				str_v_len((char const **) xde->not_show_in));
	}
}

void xdesktop_entry_set_only_in_rox(DesktopEntry *xde, gboolean state)
{
	g_strfreev(xde->only_show_in);
	xde->only_show_in = NULL;
	g_strfreev(xde->not_show_in);
	xde->not_show_in = NULL;
	if (state)
	{
		xde->only_show_in = g_new0(char *, 2);
		xde->only_show_in[0] = g_strdup("ROX");
	}
	update_kf_from_show_ins(xde);
}

static void remove_rox_from_strv(char ***p_str_v)
{
	if (!str_v_is_empty(*p_str_v))
	{
		char **ps;
		gboolean moving = FALSE;

		for (ps = *p_str_v; *ps; ++ps)
		{
			if (moving)
			{
				*(ps - 1) = *ps;
				*ps = NULL;
			}
			else if (!strcmp(*ps, "ROX"))
			{
				g_free(*ps);
				*ps = NULL;
				moving = TRUE;
			}
		}
		if (str_v_is_empty(*p_str_v))
		{
			g_strfreev(*p_str_v);
			*p_str_v = NULL;
		}
	}
}

static void add_rox_to_strv(char ***p_str_v, gboolean force)
{
	char **ps;

	if (!str_v_is_empty(*p_str_v))
	{
		gboolean contains_rox = FALSE;

		for (ps = *p_str_v; *ps; ++ps)
		{
			if (!strcmp(*ps, "ROX"))
			{
				contains_rox = TRUE;
				break;
			}
		}
		if (!contains_rox)
		{
			int l = str_v_len((char const **) *p_str_v);

			*p_str_v = g_renew(char *, *p_str_v, l + 2);
			(*p_str_v)[l] = g_strdup("ROX");
			(*p_str_v)[l + 1] = NULL;
		}
	}
	else if (force)
	{
		g_strfreev(*p_str_v);
		*p_str_v = g_new0(char *, 2);
		(*p_str_v)[0] = g_strdup("ROX");
	}
}

/*
static void show_strv(char **strv)
{
	char **ps;

	for (ps = strv; ps && *ps; ++ps)
	{
		g_print("%s ; ", *ps);
	}
	g_print("\n");
}
*/

void xdesktop_entry_set_start_in_rox(DesktopEntry *xde, gboolean state)
{
	if (state)
	{
		add_rox_to_strv(&xde->only_show_in, FALSE);
		remove_rox_from_strv(&xde->not_show_in);
	}
	else
	{
		remove_rox_from_strv(&xde->only_show_in);
		add_rox_to_strv(&xde->not_show_in, TRUE);
	}
	update_kf_from_show_ins(xde);
}

void xdesktop_entry_save(DesktopEntry *xde)
{
	char *buffer = NULL;
	gsize buflen = 0;
	GError *err = NULL;

	if (!xde->savable)
	{
		char *dirname = g_build_filename(g_get_user_config_dir(), "autostart",
				NULL);
		
		g_mkdir_with_parents(dirname, 0755);
		g_free(xde->pathname);
		xde->pathname = g_build_filename(dirname, xde->basename, NULL);
		xde->savable = TRUE;
		g_free(dirname);
	}
	buffer = g_key_file_to_data(xde->kf, &buflen, &err);
	if (!buffer || err)
	{
		message_dialog(NULL, GTK_MESSAGE_ERROR,
				_("Unable to prepare Desktop Entry for saving to '%s': %s"),
				xde->pathname, err ? err->message : _("unknown reason"));
		g_error_free(err);
		g_free(buffer);
		return;
	}
	if (!g_file_set_contents(xde->pathname, buffer, buflen, &err))
	{
		message_dialog(NULL, GTK_MESSAGE_ERROR,
				_("Unable to save '%s': %s"),
				xde->pathname, err ? err->message : _("unknown reason"));
		g_error_free(err);
		g_free(buffer);
		return;
	}
}

inline static char *add_desktop_extension(const char *basename)
{
	return g_strjoin(".", basename, "desktop", NULL);
}

/* basename doesn't include .desktop extension */
static char *xdg_autostart_pathname(const char *basename)
{
	char *leafname = add_desktop_extension(basename);
	char *pathname = g_build_filename(g_get_user_config_dir(), "autostart",
			leafname, NULL);

	g_free(leafname);
	return pathname;
}

/* This has side-effect of creating file if it doesn't clash,
 * so it must be overwritten or deleted */
static int filename_clash(const char *basename)
{
	static char *dir = NULL;
	char *pathname;
	const char * const *dirs;
	const char * const *d;
	int result = 0;

	dirs = g_get_system_config_dirs();
	for (d = dirs; *d; ++d)
	{
		char *leafname = add_desktop_extension(basename);

		pathname = g_build_filename(*d, "autostart", leafname, NULL);
		g_free(leafname);
		if (g_file_test(pathname, G_FILE_TEST_EXISTS))
			result = -1;
		g_free(pathname);
		if (result == -1)
			return -1;
	}
	if (!dir)
	{
		dir = g_build_filename(g_get_user_config_dir(), "autostart", NULL);
		if (g_mkdir_with_parents(dir, 0755) == -1)
		{
			message_dialog(NULL, GTK_MESSAGE_ERROR,
					_("Unable to create '%s' directory"), dir);
		}
	}
	if (!g_file_test(dir, G_FILE_TEST_IS_DIR))
		return -2;
	pathname = xdg_autostart_pathname(basename);
	result = open(pathname, O_WRONLY| O_EXCL|O_CREAT, 0644);
	if (result != -1)
		close(result);
	g_free(pathname);
	return result;
}

static char *add_number_ext(const char *basename)
{
	int l = strlen(basename);
	int n;
	int number = 1;

	for (n = l - 1; n >= 0 && isdigit(basename[n]); --n);
	/* This will also accept a trailing - with no subsequent digits,
	 * but that's OK */
	if (basename[n] == '-')
	{
		if (n != l - 1)
		{
			number = atoi(basename + n + 1) + 1;
		}
		l = n;
	}
	return g_strdup_printf("%.*s-%d", l, basename, number);
}

void xdesktop_entry_filenames_from_name(DesktopEntry *de, const char *leafname)
{
	int n;
	char *basename = NULL;
	int fd;

	if (leafname)
	{
		char *sep;

		basename = g_strdup(leafname);
		sep = strrchr(basename, '.');
		if (sep && sep[1] && !strcmp(sep + 1, "desktop"))
			*sep = 0;
	}
	else
	{
		basename = g_filename_from_utf8(de->name, -1, NULL, NULL, NULL);
	}
	if (!basename || !basename[0])
	{
		if (basename)
			g_free(basename);
		basename = g_strdup(_("rox-session-autostart"));
	}
	else
	{
		for (n = 0; basename[n]; ++n)
		{
			if (isspace(basename[n]))
				basename[n] = '-';
			else
				basename[n] = tolower(basename[n]);
		}
	}
	while ((fd = filename_clash(basename)) == -1)
	{
		char *old = basename;

		old = basename;
		basename = add_number_ext(basename);
		g_free(old);
	}
	g_free(de->basename);
	de->basename = add_desktop_extension(basename);
	g_free(de->pathname);
	de->pathname = xdg_autostart_pathname(basename);
	g_free(basename);
	de->savable = TRUE;
}

