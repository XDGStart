/*
 * By Tony Houghton, <h@realh.co.uk>.
 */

#ifndef XXDGAUTOSTART_H
#define XXDGAUTOSTART_H

#include <gtk/gtk.h>

#include "xdgautostart.h"

/* Extra DesktopEntry functionality for the capplet */

/* Doesn't close keyfile after loading */
DesktopEntry *xdesktop_entry_new(const char *pathname, const char *basename,
		gboolean savable);

gboolean xdesktop_entry_get_only_in_rox(DesktopEntry *);

gboolean xdesktop_entry_can_set_only_in_rox(DesktopEntry *);

inline static gboolean xdesktop_entry_can_set_start_in_rox(DesktopEntry *xde)
{
	return !xdesktop_entry_get_only_in_rox(xde);
}

void xdesktop_entry_set_only_in_rox(DesktopEntry *xde, gboolean state);

void xdesktop_entry_set_start_in_rox(DesktopEntry *xde, gboolean state);

void xdesktop_entry_save(DesktopEntry *);

/* Chooses a suitable filename, avoiding duplicates, and fills in the basename,
 * pathname and savable members. If leafname is NULL the filename is based on
 * the entry's name, otherwise on leafname */
void xdesktop_entry_filenames_from_name(DesktopEntry *, const char *leafname);

#endif /* XXDGAUTOSTART_H */
