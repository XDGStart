/*
 * By Tony Houghton, <h@realh.co.uk>.
 */

#ifndef XDGAUTOSTART_H
#define XDGAUTOSTART_H

#include <glib.h>

typedef struct {
	char *			name;
	char *			pathname;
	char *			basename;
	gboolean		deletable;
	gboolean		savable;
	char *			exec;
	char *			path;
	gboolean		terminal;
	char *			try_exec;
	char **			only_show_in;
	char **			not_show_in;
	GKeyFile *		kf;
} DesktopEntry;

/* Returns FALSE if invalid; you should then call desktop_entry_free/delete */
gboolean desktop_entry_load(DesktopEntry *, const char *pathname,
		const char *basename, gboolean keep_keyfile, gboolean savable);

/* Fills in some of the fields from the (already open) keyfile */
void desktop_entry_read_keyfile(DesktopEntry *);

/* Doesn't free the top-level struct */
void desktop_entry_free(DesktopEntry *);

/* Does free the top-level struct */
void desktop_entry_delete(DesktopEntry *);

/* Closes keyfile once loaded (read-only) */
DesktopEntry *desktop_entry_new(const char *pathname, const char *basename,
		gboolean savable);

/* desktop_entry_new() can be used as a constructor */
typedef DesktopEntry *(DesktopEntryConstructor)
	(const char *pathname, const char *basename, gboolean savable);

/* Whether (copy of) s is in v */
gboolean desktop_entry_str_in_strv(char **v, const char *s);

/* Returns whether entry should be started in environ
 * based on OnlyShowIn and NotShowIn */
gboolean desktop_entry_get_show_in_environ(const DesktopEntry *,
		const char *environ);

inline static gboolean desktop_entry_get_show_in_rox(const DesktopEntry *de)
{
	return desktop_entry_get_show_in_environ(de, "ROX");
}

/* Also checks Exec and TryExec */
gboolean desktop_entry_can_start_in_rox(const DesktopEntry *);


/* Scans XDG autostart directories for desktop files;
 * ctor should be similar to desktop_entry_new() */
GList *get_desktop_entries(DesktopEntryConstructor ctor);

inline static gboolean str_is_empty(const char *s)
{
	return !s || !s[0];
}

gboolean str_v_is_empty(char **sv);

/* full_name is main locale name eg "en_GB". Where there are variants you can
 * also set short_name eg "en". */

/* Names are not copied so they must not be freed by caller */
void desktop_entry_set_locale_names(const char *full_name,
		const char *short_name);

/* Sets locales as above from LANG and returns values in long_name and
 * short_name if not NULL */
void desktop_entry_init_locales(char const **long_name,
		char const **short_name);

#endif /* XDGAUTOSTART_H */
