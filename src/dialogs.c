/*
 * By Tony Houghton, <h@realh.co.uk>.
 */

#include "config.h"

#include <stdarg.h>

#include "dialogs.h"

void message_dialog(GtkWindow *parent, GtkMessageType mtype,
		const char *message, ...)
{
	va_list ap;
	char *formatted;
	GtkWidget *dialog;

	va_start(ap, message);
	formatted = g_strdup_vprintf(message, ap);
	va_end(ap);
	dialog = gtk_message_dialog_new(parent, GTK_DIALOG_MODAL, mtype,
			GTK_BUTTONS_CLOSE, "%s", formatted);
	gtk_window_set_title(GTK_WINDOW(dialog),
			_("Message from XDGStart capplet"));
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

