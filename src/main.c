/*
 * By Tony Houghton, <h@realh.co.uk>.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include <glib/gstdio.h>
#include <gtk/gtk.h>

#include "dialogs.h"
#include "xxdgautostart.h"

#define VIEW_MAX_W 512
#define VIEW_MAX_H 320

typedef struct {
	GList *			desktop_entries;
	GtkWidget *		dialog;
	GtkListStore *	list_store;
	GtkTreeView *	tree_view;
	GtkWidget *		edit_button;
	GtkWidget *		remove_button;
} DesktopEntriesData;

enum {
	ColData,			/* Not shown */
	ColName,
	ColRoxOnly,
	ColStartInRox,
	/* Rest not shown */
	ColRoxOnlySensitive,
	ColStartInRoxSensitive,

	NumCols
};

static const char *locale_name;
static const char *short_locale_name = NULL;

#if HAVE_GTK_WIDGET_SET_TOOLTIP_TEXT
inline static void set_tooltip(GtkWidget *widget, const char *text)
{
	gtk_widget_set_tooltip_markup(widget, text);
}
#else
static void set_tooltip(GtkWidget *widget, const char *text)
{
	static GtkTooltips *tooltips = NULL;

	if (!tooltips)
		tooltips = gtk_tooltips_new();
	gtk_tooltips_set_tip(tooltips, widget, text, NULL);
}
#endif

static void set_keyfile_name(GKeyFile *kf, const char *name)
{
	char *old_name = NULL;

	if (locale_name)
	{
		g_key_file_set_locale_string(kf, _DE, "Name", locale_name, name);
		old_name = g_key_file_get_string(kf, _DE, "Name", NULL);
		if (!old_name || !old_name[0])
			g_key_file_set_string(kf, _DE, "Name", name);
	}
	else
	{
		g_key_file_set_string(kf, _DE, "Name", name);
	}
	if (short_locale_name &&
			((old_name = g_key_file_get_locale_string(kf, _DE, "Name",
						 short_locale_name, NULL)) == NULL || !old_name[0]))
	{
		g_key_file_set_locale_string(kf, _DE, "Name", short_locale_name, name);
	}
	g_free(old_name);
}

static void get_entry_and_iter_from_path(DesktopEntriesData *ded,
		DesktopEntry **entry_out, GtkTreeIter *iter_out, GtkTreePath *path)
{
	GtkTreeIter iter;

	gtk_tree_model_get_iter(GTK_TREE_MODEL(ded->list_store),
			iter_out ? iter_out : &iter, path);
	if (entry_out)
	{
		gtk_tree_model_get(GTK_TREE_MODEL(ded->list_store),
			iter_out ? iter_out : &iter, ColData, entry_out, -1);
	}
}

/* If no selection returns FALSE and *entry_out is set to NULL,
 * *iter_out is unchanged
 */
static gboolean get_selected_entry_and_iter(DesktopEntriesData *ded,
		DesktopEntry **entry_out, GtkTreeIter *iter_out)
{
	GtkTreeSelection *seln =
		gtk_tree_view_get_selection(GTK_TREE_VIEW(ded->tree_view));
	GList *selected = gtk_tree_selection_get_selected_rows(seln, NULL);

	/* Only one node can be selected in this view, so just look at first
	 * selected item but be prepared to clean up after multiple just in case
	 */
	if (selected)
	{
		get_entry_and_iter_from_path(ded, entry_out, iter_out, selected->data);
		g_list_foreach(selected, (GFunc) gtk_tree_path_free, NULL);
		g_list_free(selected);
		return TRUE;
	}
	else
	{
		if (entry_out)
			*entry_out = NULL;
	}
	return FALSE;
}

static void shade_buttons_for_selection(DesktopEntriesData *ded)
{
	DesktopEntry *entry;
	gboolean selected;

	selected = get_selected_entry_and_iter(ded, &entry, NULL);
	gtk_widget_set_sensitive(ded->edit_button, selected);
	gtk_widget_set_sensitive(ded->remove_button, entry && entry->deletable);
}

static GKeyFile *new_de_keyfile(void)
{
	GKeyFile *kf = g_key_file_new();

	g_key_file_set_double(kf, _DE, "Version", 1.0);
	g_key_file_set_string(kf, _DE, "Encoding", "UTF-8");
	g_key_file_set_string(kf, _DE, "Name", "AutoStart Entry");
	set_keyfile_name(kf, _("New AutoStart Entry"));
	g_key_file_set_string(kf, _DE, "Exec", "rox");
	g_key_file_set_string(kf, _DE, "OnlyShowIn", "ROX;");
	return kf;
}

static DesktopEntry *new_desktop_entry(void)
{
	DesktopEntry *de = g_new0(DesktopEntry, 1);

	de->kf = new_de_keyfile();
	desktop_entry_read_keyfile(de);
	return de;
}

static void rox_only_toggled_cb(GtkCellRendererToggle *cell,
		const char *path, DesktopEntriesData *ded)
{
	DesktopEntry *xde = g_list_nth_data(ded->desktop_entries, atoi(path));
	gboolean active = !gtk_cell_renderer_toggle_get_active(cell);
	GtkTreeIter iter;

	g_return_if_fail(xde != NULL);
	xdesktop_entry_set_only_in_rox(xde, active);
	xdesktop_entry_save(xde);
	g_return_if_fail(gtk_tree_model_get_iter_from_string(
				GTK_TREE_MODEL(ded->list_store), &iter, path));
	gtk_list_store_set(ded->list_store, &iter,
			ColRoxOnly, active,
			ColStartInRoxSensitive, !active,
			-1);
}

static void start_in_rox_toggled_cb(GtkCellRendererToggle *cell,
		const char *path, DesktopEntriesData *ded)
{
	DesktopEntry *xde = g_list_nth_data(ded->desktop_entries, atoi(path));
	gboolean active = !gtk_cell_renderer_toggle_get_active(cell);
	GtkTreeIter iter;

	g_return_if_fail(xde != NULL);
	xdesktop_entry_set_start_in_rox(xde, active);
	xdesktop_entry_save(xde);
	g_return_if_fail(gtk_tree_model_get_iter_from_string(
				GTK_TREE_MODEL(ded->list_store), &iter, path));
	gtk_list_store_set(ded->list_store, &iter,
			ColStartInRox, active,
			ColRoxOnlySensitive, xdesktop_entry_can_set_only_in_rox(xde),
			-1);
}

static void open_rs_as_dir(GtkWindow *parent)
{
	GError *err;
	char *argv[3];

	argv[1] = g_build_filename(g_get_user_config_dir(),
			"rox.sourceforge.net", "ROX-Session", "AutoStart", NULL);
	if (!g_file_test(argv[1], G_FILE_TEST_IS_DIR))
	{
		message_dialog(parent, GTK_MESSAGE_WARNING,
				_("ROX-Session AutoStart directory ('%s') doesn't exist"),
				argv[1]);
		g_free(argv[1]);
		return;
	}
	argv[0] = g_find_program_in_path("rox");
	if (!argv[0])
	{
		message_dialog(parent, GTK_MESSAGE_ERROR,
				_("ROX-Filer ('rox') program not found"));
		return;
	}
	argv[2] = NULL;
	if (!gdk_spawn_on_screen(gdk_screen_get_default(), NULL, argv, NULL, 0,
				NULL, NULL, NULL, &err))
	{
		message_dialog(parent, GTK_MESSAGE_ERROR,
				_("Unable to run ROX-Filer: %s"),
				err ? err->message : _("unknown error"));
	}
	g_free(argv[0]);
	g_free(argv[1]);
}

static void set_entry(DesktopEntriesData *ded, DesktopEntry *xde,
		GtkTreeIter *iter)
{
	gtk_list_store_set(ded->list_store, iter,
			ColData, xde, ColName, xde->name,
			ColRoxOnly, xdesktop_entry_get_only_in_rox(xde),
			ColStartInRox, desktop_entry_get_show_in_rox(xde),
			ColRoxOnlySensitive, xdesktop_entry_can_set_only_in_rox(xde),
			ColStartInRoxSensitive,
				xdesktop_entry_can_set_start_in_rox(xde),
			-1);
}

static void add_entry_to_list_store(DesktopEntriesData *ded,
		DesktopEntry *xde)
{
	GtkTreeIter iter;

	gtk_list_store_append(ded->list_store, &iter);
	set_entry(ded, xde, &iter);
}

static int create_or_edit_dialog(DesktopEntriesData *ded,
		DesktopEntry *xde, gboolean create)
{
	GtkWidget *dialog_w = gtk_dialog_new_with_buttons(
			create ? _("New XDG AutoStart Entry") :
					_("Edit XDG AutoStart Entry"),
			GTK_WINDOW(ded->dialog),
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
			GTK_STOCK_APPLY, GTK_RESPONSE_ACCEPT,
			NULL);
	GtkDialog *dialog = GTK_DIALOG(dialog_w);
	GtkBox *vbox = GTK_BOX(dialog->vbox); 	
	GtkWidget *name_w = gtk_entry_new();
	GtkWidget *command_w = gtk_entry_new();
	GtkWidget *only_show_in = gtk_entry_new();
	GtkWidget *not_show_in = gtk_entry_new();
	GtkWidget *terminal_w = gtk_check_button_new_with_label(
			_("Run in terminal"));
	GtkTable *table = GTK_TABLE(gtk_table_new(2, 3, FALSE));
	GtkWidget *expander_w = gtk_expander_new(_("Advanced"));
	GtkExpander *expander = GTK_EXPANDER(expander_w);
	GtkTable *adv_table = GTK_TABLE(gtk_table_new(2, 3, FALSE));
	GtkWidget *label;
	int response;
	char *kfval;
	gboolean valid = FALSE;

	/* Tooltips */
	set_tooltip(name_w, _("Edit the name of the entry here"));
	set_tooltip(command_w, _("Set the command that this entry runs here"));
	set_tooltip(only_show_in, _("This is a list of desktop environments in "
			"which this entry will be started, separated (and terminated) "
			"by semicolons (';'). This option must be blank if you use the "
			"\"Don't start in\" option."));
#if HAVE_GTK_WIDGET_SET_TOOLTIP_TEXT
	set_tooltip(not_show_in, _("This is a list of desktop environments in "
			"which this entry will <b>not</b> be started, separated (and "
			"terminated) by semicolons (';'). This option must be blank if "
			"you use the \"Only start in\" option."));
#else
	set_tooltip(not_show_in, _("This is a list of desktop environments in "
			"which this entry will not be started, separated (and "
			"terminated) by semicolons (';'). This option must be blank if "
			"you use the \"Only start in\" option."));
#endif
	set_tooltip(expander_w,
			_("Click here to toggle the display of extra options"));
	set_tooltip(terminal_w,
			_("Whether the entry's command should be run in a terminal"));

	gtk_dialog_set_default_response(dialog, GTK_RESPONSE_ACCEPT);
	gtk_entry_set_activates_default(GTK_ENTRY(name_w), TRUE);
	gtk_entry_set_activates_default(GTK_ENTRY(command_w), TRUE);
	gtk_entry_set_activates_default(GTK_ENTRY(only_show_in), TRUE);
	gtk_entry_set_activates_default(GTK_ENTRY(not_show_in), TRUE);

	label = gtk_label_new(_("Name"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_table_attach(table, label, 0, 1, 0, 1,
			GTK_FILL, GTK_FILL, 6, 6);
	gtk_table_attach(table, name_w, 1, 2, 0, 1,
			GTK_FILL | GTK_EXPAND, GTK_FILL, 6, 6);
	label = gtk_label_new(_("Command"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_table_attach(table, label, 0, 1, 1, 2,
			GTK_FILL, GTK_FILL, 6, 6);
	gtk_table_attach(table, command_w, 1, 2, 1, 2,
			GTK_FILL | GTK_EXPAND, GTK_FILL, 6, 6);

	gtk_container_add(GTK_CONTAINER(expander), GTK_WIDGET(adv_table));
	label = gtk_label_new(_("Only start in"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_table_attach(adv_table, label, 0, 1, 0, 1,
			GTK_FILL, GTK_FILL, 6, 6);
	gtk_table_attach(adv_table, only_show_in, 1, 2, 0, 1,
			GTK_FILL | GTK_EXPAND, GTK_FILL, 6, 6);
	label = gtk_label_new(_("Don't start in"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_table_attach(adv_table, label, 0, 1, 1, 2,
			GTK_FILL, GTK_FILL, 6, 6);
	gtk_table_attach(adv_table, not_show_in, 1, 2, 1, 2,
			GTK_FILL | GTK_EXPAND, GTK_FILL, 6, 6);
	gtk_table_attach(adv_table, terminal_w, 0, 2, 2, 3,
			GTK_FILL | GTK_EXPAND, GTK_FILL, 6, 6);
	gtk_table_attach(table, GTK_WIDGET(expander), 0, 2, 2, 3,
			GTK_FILL | GTK_EXPAND, GTK_SHRINK, 6, 6);

	gtk_box_pack_start(vbox, GTK_WIDGET(table), TRUE, TRUE, 6);

	gtk_entry_set_text(GTK_ENTRY(name_w), xde->name);
	gtk_entry_set_text(GTK_ENTRY(command_w), xde->exec);
	kfval = g_key_file_get_string(xde->kf, _DE, "OnlyShowIn", NULL);
	gtk_entry_set_text(GTK_ENTRY(only_show_in), kfval ? kfval : "");
	g_free(kfval);
	kfval = g_key_file_get_string(xde->kf, _DE, "NotShowIn", NULL);
	gtk_entry_set_text(GTK_ENTRY(not_show_in), kfval ? kfval : "");
	g_free(kfval);
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(terminal_w),
			xde->terminal);

	gtk_widget_show_all(GTK_WIDGET(vbox));
	while (!valid)
	{
		response = gtk_dialog_run(dialog);
		if (response == GTK_RESPONSE_ACCEPT)
		{
			const char *name_str = gtk_entry_get_text(GTK_ENTRY(name_w));
			const char *command_str = gtk_entry_get_text(GTK_ENTRY(command_w));
			const char *only_show_in_str =
				gtk_entry_get_text(GTK_ENTRY(only_show_in));
			const char *not_show_in_str =
				gtk_entry_get_text(GTK_ENTRY(not_show_in));

			if (!name_str || !name_str[0])
			{
				message_dialog(GTK_WINDOW(dialog), GTK_MESSAGE_ERROR,
						_("The entry must have a name"));
				continue;
			}
			if (!command_str || !command_str[0])
			{
				message_dialog(GTK_WINDOW(dialog), GTK_MESSAGE_ERROR,
						_("The entry must have a command"));
				continue;
			}
			if (only_show_in_str && only_show_in_str[0] && 
					not_show_in_str && not_show_in_str[0])
			{
				message_dialog(GTK_WINDOW(dialog), GTK_MESSAGE_ERROR,
						_("You can't set both \"Don't start in\" and "
						"\"Only start in\""));
				continue;
			}
			set_keyfile_name(xde->kf, name_str);
			g_key_file_set_string(xde->kf, _DE, "Exec", command_str);
			if (only_show_in_str && only_show_in_str[0])
			{
				g_key_file_set_value(xde->kf, _DE, "OnlyShowIn",
						only_show_in_str);
			}
			else
			{
				g_key_file_remove_key(xde->kf, _DE, "OnlyShowIn", NULL);
			}
			if (not_show_in_str && not_show_in_str[0])
			{
				g_key_file_set_value(xde->kf, _DE, "NotShowIn",
						not_show_in_str);
			}
			else
			{
				g_key_file_remove_key(xde->kf, _DE, "NotShowIn", NULL);
			}
			g_key_file_set_boolean(xde->kf, _DE, "Terminal",
				gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(terminal_w)));
			valid = TRUE;
		}
		else
		{
			valid = TRUE;
		}
	}
	if (response != GTK_RESPONSE_DELETE_EVENT)
		gtk_widget_destroy(dialog_w);
	return response;
}

static void new_entry_dialog_for_entry(DesktopEntriesData *ded,
		DesktopEntry *xde)
{
	if (create_or_edit_dialog(ded, xde, TRUE) == GTK_RESPONSE_ACCEPT)
	{
		desktop_entry_read_keyfile(xde);
		xdesktop_entry_filenames_from_name(xde, NULL);
		xdesktop_entry_save(xde);
		ded->desktop_entries = g_list_append(ded->desktop_entries, xde);
		xde->deletable = TRUE;
		add_entry_to_list_store(ded, xde);
		shade_buttons_for_selection(ded);
	}
	else
	{
		desktop_entry_delete(xde);
	}
}

static void new_entry_cb(DesktopEntriesData *ded)
{
	new_entry_dialog_for_entry(ded, new_desktop_entry());
}

static void edit_entry(DesktopEntriesData *ded, DesktopEntry *xde,
		GtkTreeIter *iter)
{
	if (create_or_edit_dialog(ded, xde, FALSE) == GTK_RESPONSE_ACCEPT)
	{
		desktop_entry_read_keyfile(xde);
		xdesktop_entry_save(xde);
		set_entry(ded, xde, iter);
		shade_buttons_for_selection(ded);
	}
}

static void edit_entry_cb(DesktopEntriesData *ded)
{
	DesktopEntry *xde;
	GtkTreeIter iter;

	g_return_if_fail(get_selected_entry_and_iter(ded, &xde, &iter));
	edit_entry(ded, xde, &iter);
}

static void remove_entry_cb(DesktopEntriesData *ded)
{
	DesktopEntry *xde;
	GtkTreeIter iter;

	g_return_if_fail(get_selected_entry_and_iter(ded, &xde, &iter));
	gtk_list_store_remove(ded->list_store, &iter);
	ded->desktop_entries = g_list_remove(ded->desktop_entries, xde);
	shade_buttons_for_selection(ded);
	g_unlink(xde->pathname);
	desktop_entry_delete(xde);
}

static void row_activated_cb(GtkTreeView *tree_view, GtkTreePath *path,
		GtkTreeViewColumn *column, DesktopEntriesData *ded)
{
	DesktopEntry *xde;
	GtkTreeIter iter;

	get_entry_and_iter_from_path(ded, &xde, &iter, path);
	edit_entry(ded, xde, &iter);
}

static DesktopEntriesData *build_dialog(GList *desktop_entries)
{
	DesktopEntriesData *ded = g_new0(DesktopEntriesData, 1);
	GList *node;
	GtkCellRenderer *cell;
	GtkTreeViewColumn *column;
	GtkWidget *tvwidget;
	GtkRequisition req;
	GtkWidget *scroll;
	GtkWidget *btn_hbox;
	GtkWidget *btn_hbox2;
	GtkBox *vbox;
	GtkWidget *misc_w;

	ded->desktop_entries = desktop_entries;
	ded->dialog = gtk_dialog_new_with_buttons(_("XDG AutoStart Entries"),
			NULL, 0, GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);

	/* Set up list store */
	ded->list_store = gtk_list_store_new(NumCols,
			G_TYPE_POINTER, G_TYPE_STRING, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN,
			G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN, G_TYPE_BOOLEAN);
	for (node = ded->desktop_entries; node; node = g_list_next(node))
		add_entry_to_list_store(ded, node->data);

	/* Set up tree view */
	tvwidget = gtk_tree_view_new_with_model(
			GTK_TREE_MODEL(ded->list_store));
	ded->tree_view = GTK_TREE_VIEW(tvwidget);
	g_signal_connect_swapped(ded->tree_view, "cursor-changed",
			G_CALLBACK(shade_buttons_for_selection), ded);
	g_signal_connect(ded->tree_view, "row-activated",
			G_CALLBACK(row_activated_cb), ded);
	set_tooltip(tvwidget, _("The entries here are implemented as dektop "
			"entry files according to the XDG specification"));

	cell = gtk_cell_renderer_text_new();
	column = gtk_tree_view_column_new_with_attributes(_("Name"), cell,
			"text", ColName, NULL);
	gtk_tree_view_append_column(ded->tree_view, column);

	cell = gtk_cell_renderer_toggle_new();
	column = gtk_tree_view_column_new_with_attributes(_("Only in ROX"), cell,
			"active", ColRoxOnly,
			"activatable", ColRoxOnlySensitive,
			"sensitive", ColRoxOnlySensitive,
			NULL);
	gtk_tree_view_append_column(ded->tree_view, column);
	g_signal_connect(cell, "toggled", G_CALLBACK(rox_only_toggled_cb), ded);

	cell = gtk_cell_renderer_toggle_new();
	column = gtk_tree_view_column_new_with_attributes(_("Start in ROX"), cell,
			"active", ColStartInRox,
			"activatable", ColStartInRoxSensitive,
			"sensitive", ColStartInRoxSensitive,
			NULL);
	gtk_tree_view_append_column(ded->tree_view, column);
	g_signal_connect(cell, "toggled", G_CALLBACK(start_in_rox_toggled_cb), ded);

	/* Create buttons */
	btn_hbox = gtk_hbox_new(TRUE, 16);
	ded->remove_button = gtk_button_new_from_stock(GTK_STOCK_DELETE);
	g_signal_connect_swapped(ded->remove_button, "clicked",
			G_CALLBACK(remove_entry_cb), ded);
	gtk_box_pack_start_defaults(GTK_BOX(btn_hbox), ded->remove_button);
	ded->edit_button = gtk_button_new_from_stock(GTK_STOCK_EDIT);
	g_signal_connect_swapped(ded->edit_button, "clicked",
			G_CALLBACK(edit_entry_cb), ded);
	gtk_box_pack_start_defaults(GTK_BOX(btn_hbox), ded->edit_button);
	misc_w = gtk_button_new_from_stock(GTK_STOCK_ADD);
	g_signal_connect_swapped(misc_w, "clicked", G_CALLBACK(new_entry_cb), ded);
	gtk_box_pack_start_defaults(GTK_BOX(btn_hbox), misc_w);

	/* Lay out main TreeView widget */
	gtk_widget_size_request(tvwidget, &req);
	if (req.width < VIEW_MAX_W)
		req.width += 32;
	if (req.width > VIEW_MAX_W)
		req.width = VIEW_MAX_W;
	if (req.height < VIEW_MAX_H)
		req.height += 32;
	if (req.height > VIEW_MAX_H)
		req.height = VIEW_MAX_H;
	scroll = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scroll),
			GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scroll),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_container_add(GTK_CONTAINER(scroll), tvwidget);
	gtk_widget_set_size_request(tvwidget, req.width, req.height);

	btn_hbox2 = gtk_hbox_new(FALSE, 16);
	misc_w = gtk_button_new_with_label(
			_("Open ROX-Session AutoStart directory"));
	set_tooltip(misc_w, _("At start-up ROX-Session also automatically runs "
			"any scripts, executables or ROX applications in its AutoStart "
			"directory which can be opened with this button"));
	g_signal_connect_swapped(misc_w, "clicked",
			G_CALLBACK(open_rs_as_dir), ded->dialog);
	gtk_box_pack_end(GTK_BOX(btn_hbox2), misc_w, FALSE, FALSE, 0);

	/* Do master lay-out and show everything */
	vbox = GTK_BOX(GTK_DIALOG(ded->dialog)->vbox);
	gtk_box_pack_start(vbox, scroll, TRUE, TRUE, 6);
	gtk_box_pack_start(vbox, btn_hbox, FALSE, FALSE, 6);
	gtk_box_pack_start(vbox, btn_hbox2, FALSE, FALSE, 6);
	shade_buttons_for_selection(ded);
	gtk_widget_show_all(GTK_WIDGET(vbox));

	return ded;
}

int main(int argc, char **argv)
{
	GList *desl;
	DesktopEntriesData *ded;

	desktop_entry_init_locales(&locale_name, &short_locale_name);
	gtk_init(&argc, &argv);
	desl = get_desktop_entries((DesktopEntryConstructor *) xdesktop_entry_new);
	ded = build_dialog(desl);
	if (gtk_dialog_run(GTK_DIALOG(ded->dialog)) != GTK_RESPONSE_DELETE_EVENT)
		gtk_widget_destroy(ded->dialog);

	return 0;
}

