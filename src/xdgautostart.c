/*
 * By Tony Houghton, <h@realh.co.uk>.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "xdgautostart.h"

#ifndef _DE
#define _DE "Desktop Entry"
#endif

static const char *locale_name = NULL;
static const char *short_locale_name = NULL;

static void desktop_entry_free_keyfile_mirrors(DesktopEntry *de)
{
	g_free(de->name);
	de->name = NULL;
	g_free(de->exec);
	de->exec = NULL;
	g_free(de->path);
	de->path = NULL;
	g_free(de->try_exec);
	de->try_exec = NULL;
	g_strfreev(de->only_show_in);
	de->only_show_in = NULL;
	g_strfreev(de->not_show_in);
	de->not_show_in = NULL;
}

static void desktop_entry_read_name_from_keyfile(DesktopEntry *xde)
{
	g_free(xde->name);
	if (locale_name)
	{
		xde->name = g_key_file_get_locale_string(xde->kf, _DE, "Name",
				locale_name, NULL);
	}
	if (!xde->name && short_locale_name)
	{
		xde->name = g_key_file_get_locale_string(xde->kf, _DE, "Name",
				short_locale_name, NULL);
	}
	if (!xde->name)
		xde->name = g_key_file_get_string(xde->kf, _DE, "Name", NULL);
	if (!xde->name)
		xde->name = g_strdup(_("No name"));
}

void desktop_entry_read_keyfile(DesktopEntry *de)
{
	GKeyFile *kf = de->kf;

	desktop_entry_free_keyfile_mirrors(de);
	desktop_entry_read_name_from_keyfile(de);
	de->exec = g_key_file_get_string(kf, _DE, "Exec", NULL);
	de->path = g_key_file_get_string(kf, _DE, "Path", NULL);
	de->terminal = g_key_file_get_boolean(kf, _DE, "Terminal", NULL);
	de->try_exec = g_key_file_get_string(kf, _DE, "TryExec", NULL);
	de->only_show_in = g_key_file_get_string_list(kf, _DE, "OnlyShowIn",
			NULL, NULL);
	de->not_show_in = g_key_file_get_string_list(kf, _DE, "NotShowIn",
			NULL, NULL);
}

gboolean desktop_entry_load(DesktopEntry *de, const char *pathname,
		const char *basename, gboolean keep_keyfile, gboolean savable)
{
	GKeyFile *kf = g_key_file_new();
	GError *err = NULL;

	de->kf = kf;
	if (!g_key_file_load_from_file(kf, pathname,
			keep_keyfile ?
				G_KEY_FILE_KEEP_COMMENTS | G_KEY_FILE_KEEP_TRANSLATIONS :
				G_KEY_FILE_NONE,
			&err))
	{
		g_warning(_("Can't load key-file '%s': %s"), pathname,
				err ? err->message : _("unknown reason"));
		g_error_free(err);
		return FALSE;
	}
	if (!g_key_file_has_group(kf, _DE))
	{
		g_warning(_("'%s' is not a .desktop file"), pathname);
		return FALSE;
	}

	de->pathname = g_strdup(pathname);
	de->basename = g_strdup(basename);
	de->savable = savable;
	de->deletable = savable;

	desktop_entry_read_keyfile(de);

	if (!keep_keyfile)
	{
		g_key_file_free(kf);
		de->kf = NULL;
	}
	return TRUE;
}

void desktop_entry_free(DesktopEntry *de)
{
	desktop_entry_free_keyfile_mirrors(de);
	g_free(de->pathname);
	de->pathname = NULL;
	g_free(de->basename);
	de->basename = NULL;
	if (de->kf)
	{
		g_key_file_free(de->kf);
		de->kf = NULL;
	}
}

void desktop_entry_delete(DesktopEntry *de)
{
	desktop_entry_free(de);
	g_free(de);
}

DesktopEntry *desktop_entry_new(const char *pathname, const char *basename,
		gboolean savable)
{
	DesktopEntry *de = g_new0(DesktopEntry, 1);

	if (!desktop_entry_load(de, pathname, basename, FALSE, savable))
	{
		desktop_entry_delete(de);
		de = NULL;
	}
	return de;
}

gboolean desktop_entry_str_in_strv(char **v, const char *s)
{
	for ( ; v && *v; ++v)
	{
		if (!strcmp(*v, s))
			return TRUE;
	}
	return FALSE;
}

gboolean desktop_entry_get_show_in_environ(const DesktopEntry *de,
		const char *environ)
{
	if (de->only_show_in)
	{
		return desktop_entry_str_in_strv(de->only_show_in, environ);
	}
	if (de->not_show_in)
	{
		if (desktop_entry_str_in_strv(de->not_show_in, environ))
			return FALSE;
	}
	return TRUE;
}

/* Also checks Exec and TryExec */
gboolean desktop_entry_can_start_in_rox(const DesktopEntry *de)
{
	if (!desktop_entry_get_show_in_rox(de))
	{
		return FALSE;
	}
	if (!de->exec)
	{
		return FALSE;
	}
	if (de->try_exec)
	{
		if (!g_file_test(de->try_exec, G_FILE_TEST_EXISTS))
			return FALSE;
	}
	if (de->exec[0] == '/')
	{
		return g_file_test(de->exec, G_FILE_TEST_IS_EXECUTABLE);
	}
	else
	{
		char *exec = g_strdup(de->exec);
		char *spc = strchr(exec, ' ');
		char *ef;
		gboolean result;

		if (spc)
			*spc = 0;
		spc = strchr(exec, '\t');
		if (spc)
			*spc = 0;
		ef = g_find_program_in_path(exec);
		result = ef != NULL;

		g_free(exec);
		g_free(ef);
		return result;
	}
	return FALSE;
}

static GList *scan_dir(const char *dir, GList *desl,
		DesktopEntryConstructor ctor, gboolean savable)
{
	char *dir2 = g_build_filename(dir, "autostart", NULL);
	GDir *dir0;
	const char *basename;

	if (!g_file_test(dir2, G_FILE_TEST_IS_DIR))
		goto no_dir;
	dir0 = g_dir_open(dir2, 0, NULL);
	if (!dir0)
		goto no_dir;
	while ((basename = g_dir_read_name(dir0)) != NULL)
	{
		GList *node;
		gboolean dupl = FALSE;

		/* Don't add if there's already one with the same basename */
		for (node = desl; node; node = g_list_next(node))
		{
			DesktopEntry *de = node->data;
			
			if (!strcmp(basename, de->basename))
			{
				dupl = TRUE;
				break;
			}
		}
		if (!dupl)
		{
			char *pathname = g_build_filename(dir2, basename, NULL);
			DesktopEntry *de = ctor(pathname, basename, savable);

			if (de)
				desl = g_list_append(desl, de);
			g_free(pathname);
		}
	}

	g_dir_close(dir0);
no_dir:
	g_free(dir2);
	return desl;
}

GList *get_desktop_entries(DesktopEntryConstructor ctor)
{
	GList *desl = NULL;
	const char *udir = g_get_user_config_dir();
	const char * const * sdirs;

	desl = scan_dir(udir, desl, ctor, TRUE);
	for (sdirs = g_get_system_config_dirs(); sdirs && *sdirs; ++sdirs)
	{
		desl = scan_dir(*sdirs, desl, ctor, FALSE);
	}
	return desl;
}

gboolean str_v_is_empty(char **sv)
{
	if (!sv)
		return TRUE;
	for ( ; *sv; ++sv)
	{
		if (*sv)
			return FALSE;
	}
	return TRUE;
}

void desktop_entry_set_locale_names(const char *full_name,
		const char *short_name)
{
	locale_name = full_name;
	short_locale_name = short_name;
}

void desktop_entry_init_locales(char const **long_name, char const **short_name)
{
	char *lang = getenv("LANG");

	if (lang)
	{
		char *sep = strchr(lang, '.');
		int l = sep ? sep - lang : strlen(lang);
		
		locale_name = g_strdup_printf("%.*s", l, lang);
		sep = strchr(locale_name, '_');
		if (sep)
		{
			short_locale_name = g_strdup_printf("%.*s",
					(int) (sep - locale_name), locale_name);
		}
	}
	else
	{
		locale_name = NULL;
	}
	if (long_name)
		*long_name = locale_name;
	if (short_name)
		*short_name = short_locale_name;
}

